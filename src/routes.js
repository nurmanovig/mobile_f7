import Home from './pages/home.vue'
import Shop from './pages/shop.vue'
import Account from './pages/account.vue'
import Store from './pages/store.vue'
import News from './pages/news.vue'
import QrCode from './pages/qrcode.vue'
export default [
  {
    path: '/',
    component: Home
  },
  {
    path: '/shop',
    component: Shop
  },
  {
    path: '/account',
    component: Account
  },
  {
    path: '/news',
    component: News
  },
  {
    path: '/store',
    component: Store
  },
  {
    path: '/qrcode',
    component: QrCode
  }
]
