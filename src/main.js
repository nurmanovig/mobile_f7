import Vue from 'vue'
import App from './App.vue'
import Framework7 from 'framework7/framework7.esm.bundle.js'
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js'
// Import F7 Styles
import 'framework7/css/framework7.css'
// Import Icons and App Custom Styles
import './css/icons.css'
import './css/app.css'
import 'framework7-icons'
// only import the icons you use to reduce bundle size
import 'vue-awesome/icons/flag'
// or import all icons if you don't care about bundle size
import 'vue-awesome/icons'
/* Register component with one of 2 methods */
import Icon from 'vue-awesome/components/Icon'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAMKfWztm3PRZw5oXvmJLZnLxr-Vl7okps',
    libraries: 'places' // This is required if you use the Autocomplete plugin

  }
})

// globally (in your main .js file)
Vue.component('v-icon', Icon)

Framework7.use(Framework7Vue)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
