// Import Vue
import Vue from 'vue'

// Import F7
import Framework7 from 'framework7/framework7.esm.bundle.js'

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js'

// Import App Component
import App from './app.vue'
import QRScanner from "cordova-plugin-qrscanner"
// Init F7 Vue Plugin
Framework7.use(Framework7Vue)
Vue.use(QRScanner)
// Init App
Vue({
  el: '#app',
  template: '<app/>',

  // Register App Component
  components: {
    app: App
  }
})
